install: yarn (if not installed -> https://yarnpkg.com/en/);

development: yarn start;

production: yarn build;


build config: './build/config.js';

const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const CONFIG = require('./config')
const DEV = process.env.NODE_ENV === 'development'
const PROD = process.env.NODE_ENV === 'production'

module.exports = {
  entry: {
    main: ['babel-polyfill', CONFIG.path.entry]
  },
  output: {
    path: CONFIG.path.dist,
    filename: CONFIG.name.build,
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /(\.tpl|\.html)$/,
        loader: 'lodash-template-webpack-loader'
      },
      {
        enforce: "pre",
        test: /\.js$/,
        exclude:  /(node_modules|bower_components)/,
        loader: 'eslint-loader'
      },
      {
        test: /\.js$/,
        exclude:  /(node_modules|bower_components)/,
        loader: 'babel-loader'
      },
      {
        test: /\.(png|jpg|gif|ttf|eot|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: CONFIG.name.asset
        }
      },
      {
        test: /\.svg/,
        loader: 'svg-inline-loader'
      }
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      DEV: DEV,
      PROD: PROD
    }),
    new HtmlWebpackPlugin({
      title: CONFIG.template.title,
      template: CONFIG.path.template,
      description: CONFIG.template.description,
      author: CONFIG.template.author,
      keywords: CONFIG.template.keywords,
      minify: {
        removeAttributeQuotes: true,
        collapseWhitespace: true,
        html5: true,
        removeComments: true,
        removeEmptyAttributes: true
      }
    })
  ]

}

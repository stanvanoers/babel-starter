const path = require('path')
const ROOT = path.resolve(__dirname, '..')

module.exports = {
  server: {
    local: true,
    port: 3001
  },
  template: {
    title: 'Application title',
    keywords: 'Some, keywords, heere',
    author: 'Stan van Oers - stanvanoers@gmail.com',
    description: 'Application description'
  },
  path: {
    src: path.resolve(ROOT, 'src'),
    entry: path.resolve(ROOT, 'src', 'index.js'),
    template: path.resolve(ROOT, 'src', 'index.html'),
    dist: path.join(ROOT, 'dist')
  },
  name: {
    build: 'build.[hash].js',
    styles: 'styles.[hash].css',
    asset: '[hash].[ext]',
    modules: {
      dev: '[name]__[local]--[hash:base64:5]',
      prod: '[hash:base64]'
    }
  }
}

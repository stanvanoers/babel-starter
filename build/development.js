const webpackConfig = require('./common')
const webpack = require('webpack')
const CONFIG = require('./config')

webpackConfig.devtool = 'eval'
webpackConfig.module.rules.push({
  test: /\.scss$/,
  use: [
    {
      loader: "style-loader"
    },
    {
      loader: "css-loader",
      options: {
        modules: true,
        localIdentName: CONFIG.name.modules.dev
      }
    },
    { loader: "sass-loader" }
  ]
})

webpackConfig.entry.main.push(
  `webpack-hot-middleware/client.js?path=${webpackConfig.output.publicPath}__webpack_hmr&reload=true`
)

webpackConfig.plugins.push(
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin(),
  new webpack.NoEmitOnErrorsPlugin()
)

module.exports = webpackConfig

const webpackConfig = require('./common')
const CONFIG = require('./config')
const webpack = require('webpack')

const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

webpackConfig.devtool = false
webpackConfig.plugins.push(new UglifyJSPlugin())

/**
 *
 *    Styles
 */
const extractSass = new ExtractTextPlugin({
  filename: CONFIG.name.styles,
  allChunks: true
})

webpackConfig.module.rules.push({
  test: /\.scss$/,
  use: extractSass.extract({
    use: [
      { loader: "css-loader",
        options: {
          minimize: true,
          modules: true,
          localIdentName: CONFIG.name.modules.prod
        }
      },
      { loader: "autoprefixer-loader" },
      { loader: "sass-loader" }
    ],
    fallback: "style-loader"
  })
})

webpackConfig.plugins.push(
  extractSass
)

module.exports = webpackConfig

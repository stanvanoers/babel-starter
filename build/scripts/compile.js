const chalk = require('chalk')
const log = console.log

const config = require('../production')
const webpack = require('webpack')
const compiler = webpack(config)

compiler.run(function(err, stats) {
  if (err) log(chalk.red.bold('Webpack compiler encountered a fatal error.', err))
  else {
    const jsonStats = stats.toJson()
    if (jsonStats.errors.length > 0) {
      log(chalk.red.bold('Webpack compiler encountered errors.'))
      log(jsonStats.errors.join('\n'))
    } else if (jsonStats.warnings.length > 0) {
      log(chalk.yellow.bold('Webpack compiler encountered warnings.'))
      log(jsonStats.warnings.join('\n'))
    } else {
      log(chalk.green.bold('Build is successful.'))
      log(stats.toString({
        colors: true,
        chunks: false,
      }))
    }
  }
})



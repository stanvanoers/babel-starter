const express = require('express')
const path = require('path')
const webpack = require('webpack')
const webpackConfig = require('../development')
const config = require('../config')
const compress = require('compression')
const ip = require('my-local-ip')
const chalk = require('chalk')
const app = express()
app.use(compress())

const compiler = webpack(webpackConfig)
  
app.use(require('webpack-dev-middleware')(compiler, {
  publicPath  : webpackConfig.output.publicPath,
  contentBase : path.resolve(config.path.src),
  hot         : true,
  quiet       : false,
  noInfo      : false,
  lazy        : false,
  stats       : {
    colors: true,
  },
}))

app.use(require('webpack-hot-middleware')(compiler, {
  path: '/__webpack_hmr'
}))

app.use('*', function (req, res, next) {
  const filename = path.join(config.path.dist, 'index.html')
  compiler.outputFileSystem.readFile(filename, (err, result) => {
    if (err) {
      console.log(err)
      return next(err)
    }
    res.set('content-type', 'text/html')
    res.send(result)
    res.end()
  })
})

const adress = config.server.local ? 'localhost': ip()

app.listen(config.server.port, adress, function () {
  console.log(chalk.green.bold('Development running on ' + adress + ':' + config.server.port))
})

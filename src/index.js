import classNames from 'classnames/bind';
import styles from './styles/main.scss';
import data from './data/hello.json';
import template from './templates/hello.html';
import image from './assets/images/sheep.jpg';

const createNode = (templateString) => {
  const node = document.createElement('div');
  node.innerHTML = templateString;
  return node;
}

const mount = (element, renderedTemplate) => {
  // Remove children
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
  // Append node
  const node = createNode(renderedTemplate);
  element.appendChild(node);
}

const MOUNT_ELEMENT = document.body;
mount(MOUNT_ELEMENT, template({
  ...data,
  image: image,
  styles: classNames.bind(styles)
}))
